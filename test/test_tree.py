import numpy as np
import bope

def test_tree():
  X_train = np.random.normal(size=(129, 5)).astype('float32')
  X_test = np.random.normal(size=(65, 5)).astype('float32')

  dt = bope.tree.DecisionTree(3)
  bope.tree.dummy_train_tree(dt, X_train)
  predictions = dt.predict(X_test)

  assert predictions.shape == (X_test.shape[0], )

