# boosted-ope

OPE for boosting

## Installation

To install the package: 
```sh
### in the root of the repository
pip install -e .
```

Changes in the Python code do not require reinstallation, however, for changes in the Cython code to have an affect the package needs to be recompiled:
```sh
### in the root of the repository
python setup.py build_ext --inplace
``` 


To launch tests:
```sh
### in the root of the repository
pytest test/
```

