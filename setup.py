from setuptools import setup, find_packages, Extension
import os

def split_libs(var):
  return [
    path for path in var.split(':') if len(path) > 0
  ]

def get_includes():
  import numpy

  env = os.environ

  includes = []

  for k in ['CPATH', 'C_INCLUDE_PATH', 'INCLUDE_PATH']:
    if k in env:
      includes.extend(split_libs(env[k]))

  includes.append(numpy.get_include())

  return includes

def get_library_dirs():
  env = os.environ

  libs = []

  for k in ['LD_LIBRARY_PATH']:
    if k in env:
      libs.extend(split_libs(env[k]))

  return libs

from Cython.Build import cythonize

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8', mode='r') as f:
  long_description = f.read()

extra_compile_args=['-Ofast', '-g']
extra_link_args=['-g']

extensions = [
  Extension(
    'bope.tree.tree', [
      'bope/tree/tree.pyx',
    ],
    include_dirs=get_includes(),
    library_dirs=get_library_dirs(),
    language='c',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  )
]

setup(
  name='bope',

  version='0.0.1',

  description="""Boosted OPE""",

  long_description = long_description,

  url='https://gitlab.com/lambda-hse/boosted-ope/',

  author='Maxim Borisyak',
  author_email='maximus.been at gmail com',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'maximus.been at gmail com',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3',
  ],

  packages=find_packages(),

  extras_require={
    'test': ['pytest >= 5.0.0'],
  },

  install_requires=[
    'cython',
  ],

  ext_modules = cythonize(
    extensions,
    gdb_debug=True,
    compiler_directives={
      'embedsignature': True,
      'language_level' : 3
    }
  ),
)
