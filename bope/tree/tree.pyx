import numpy as np
cimport numpy as np

ctypedef np.int_t INDEX_t
ctypedef np.float32_t FEATURE_t
ctypedef np.float32_t PREDICTION_t

cdef class DecisionTree:
  cdef int depth
  cdef int n_leaves
  cdef int n_splits

  cdef INDEX_t[:] feature_indexes
  cdef FEATURE_t[:] thresholds

  cdef PREDICTION_t[:] predictions

  def __init__(self, int depth):
    self.n_splits = 2 ** (depth - 1) - 1
    self.feature_indexes = np.zeros(shape=(self.n_splits, ), dtype='int')
    self.thresholds = np.zeros(shape=(self.n_splits, ), dtype='float32')

    self.n_leaves = 2 ** (depth  - 1)
    self.predictions = np.zeros(shape=(self.n_leaves, ), dtype='float32')

  cdef PREDICTION_t predict_single(self, np.ndarray[FEATURE_t, ndim=1] sample):
    cdef int i, split_index
    cdef INDEX_t feature
    cdef FEATURE_t threshold

    ## root node
    split_index = 0

    for i in range(self.depth - 1):
      feature = self.feature_indexes[split_index]
      threshold = self.thresholds[split_index]

      # see, https://en.wikipedia.org/wiki/Binary_tree#Arrays
      # if sample[feature] > threshold:
      #   split_index = 2 * split_index + 1
      # else:
      #   split_index = 2 * split_index + 2

      split_index = 2 * split_index + (1 if sample[feature] > threshold else 2)

    cdef int prediction_index = split_index - self.n_splits
    return self.predictions[prediction_index]

  cpdef np.ndarray[PREDICTION_t, ndim=1] predict(self, np.ndarray[FEATURE_t, ndim=2] X):
    cdef np.ndarray[PREDICTION_t, ndim=1] predictions = np.ndarray(shape=(len(X), ), dtype='float32')
    cdef int i

    for i in range(len(X)):
      predictions[i] = self.predict_single(X[i])

    return predictions

cpdef void dummy_train_tree(DecisionTree tree, np.ndarray[FEATURE_t, ndim=2] X_train):
  ### this is a dummy implementation

  cdef int n_samples = X_train.shape[0]
  cdef int n_features = X_train.shape[1]
  cdef int i

  for i in range(len(tree.feature_indexes)):
    tree.feature_indexes[i] = i % n_features
    tree.thresholds[i] = X_train[i % n_samples, i % n_features]

  for i in range(len(tree.predictions)):
    tree.predictions[i] = X_train[i % n_samples, i % n_features]

cpdef void cross_entropy_train(DecisionTree tree, np.ndarray[FEATURE_t, ndim=2] X_train):
  pass
